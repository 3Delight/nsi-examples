#include <nsi.hpp>

int main(  )
{
	NSI::Context nsi;
	nsi.Begin();

	// Camera transform
	nsi.Create( "cam_trs", "transform" );
	double cam[16] =
	{
		1, 0, 0, 0,
		0, 1, 0, 0,
		0, 0, -1, 0,
		0, 0, 0, 1
	};

	nsi.SetAttribute(
		"cam_trs",
		NSI::DoubleMatrixArg( "transformationmatrix", cam) );
	nsi.Connect( "cam_trs", "", NSI_SCENE_ROOT, "objects" );

	/* Perspective Camera Parameters */
	nsi.Create( "maincamera", "perspectivecamera" );
	nsi.SetAttribute( "maincamera", NSI::FloatArg( "fov", 100) );
	nsi.Connect( "maincamera", "", "cam_trs", "objects" );

	/* Camera's screen */
	nsi.Create( "raster", "screen" );
	nsi.SetAttribute( "raster", NSI::IntegerArg("oversampling", 8 ) );
	nsi.Connect( "raster", "", "maincamera", "screens" );

	/* Output drivers  */
	nsi.Create( "driver", "outputdriver" );
	nsi.Create( "driver2", "outputdriver" );
	nsi.SetAttribute( "driver",
		(
			NSI::StringArg( "drivername", "idisplay" ),
			NSI::StringArg( "imagefilename", "area_lights" )
		) );

	nsi.SetAttribute( "driver2",
		(
			NSI::StringArg( "drivername", "exr" ),
			NSI::StringArg( "imagefilename", "area_lights.exr" )
		) );

	/* The layer (data types, naming, etc) */
	nsi.Create( "beautylayer", "outputlayer" );
	nsi.Create( "zbuffer", "outputlayer" );
	nsi.SetAttribute( "beautylayer",
		(
			NSI::StringArg( "variablename", "Ci" ),
			NSI::StringArg( "channelname", "beauty" ),
			NSI::StringArg( "scalarformat", "half" ),
			NSI::IntegerArg( "withalpha", 1 )
		) );

	nsi.SetAttribute( "zbuffer",
		(
			NSI::StringArg( "variablename", "z" ),
			NSI::StringArg( "variablesource", "builtin" ),
			NSI::StringArg( "layertype", "scalar" ),
			NSI::StringArg( "channelname", "z depth" ),
			NSI::StringArg( "scalarformat", "float" )
		) );

	nsi.Connect( "beautylayer", "", "raster", "outputlayers" );
	nsi.Connect( "zbuffer", "", "raster", "outputlayers" );

	nsi.Connect( "driver", "", "beautylayer", "outputdrivers" );
	nsi.Connect( "driver2", "", "beautylayer", "outputdrivers" );

	nsi.Connect( "driver", "", "zbuffer", "outputdrivers" );
	nsi.Connect( "driver2", "", "zbuffer", "outputdrivers" );

	/* main scene transform */
	double anchor[16] =
	{
		1, 0, 0, 0,
		0, 1, 0, 0,
		0, 0, 1, 0,
		0, 0, 2, 1
	};

	nsi.Create( "anchor", "transform" );
	nsi.SetAttribute( "anchor",
		NSI::DoubleMatrixArg( "transformationmatrix", anchor ) );
	nsi.Connect( "anchor", "", ".root", "objects" );

	/* Red quads */
	int nvertices_quads[2] = { 4, 4 };
	float p[] = {
		-1.5, 0, 0.5,  -0.5, 0, 0.5,  -1.5, -1, 0.5,  -0.5, -1, 0.5,
		0.5, 0, 0.5,  1.5, 0, 0.5,  0.5, -1, 0.5,  1.5, -1, 0.5
	};
	int indices[] = { 0, 1, 3, 2, 4, 5, 7, 6 };

    NSI::ArgumentList red_mesh_args; /* passing variables lists */
	red_mesh_args.Add(
		NSI::Argument::New( "nvertices" )
			->SetType( NSITypeInteger )
			->SetCount( sizeof(nvertices_quads)/sizeof(nvertices_quads[0]) )
			->SetValuePointer( nvertices_quads ) );

    red_mesh_args.Add(
		NSI::Argument::New( "P.indices" )
        	->SetType( NSITypeInteger )
        	->SetCount( sizeof(indices)/sizeof(indices[0]) )
        	->SetValuePointer( indices ) );
	red_mesh_args.Add(
		NSI::Argument::New( "P" )
        	->SetType( NSITypePoint )
        	->SetCount( sizeof(p) / sizeof(p[0]) )
        	->SetValuePointer( const_cast<float*>(p) ) );

	nsi.Create( "red", "mesh" );
	nsi.SetAttribute( "red", red_mesh_args );
	nsi.Connect( "red", "", "anchor", "objects" );

	/* blue quads, reuse nvertices and indices from red quads  */
    NSI::ArgumentList blue_mesh_args;
	float p2[] = {
		-1.3, -0.2, -0.8,  -0.5, -0.2, -0.8,  -1.3, -1, -0.8,  -0.5, -1, -0.8,
		0.5, -0.2, -0.8,  1.3, -0.2, -0.8,  0.5, -1, -0.8,  1.3, -1, -0.8 };
	blue_mesh_args.Add(
		NSI::Argument::New( "nvertices" )
			->SetType( NSITypeInteger )
			->SetCount( sizeof(nvertices_quads)/sizeof(nvertices_quads[0]) )
			->SetValuePointer( nvertices_quads) );
    blue_mesh_args.Add(
		NSI::Argument::New( "P.indices" )
        	->SetType( NSITypeInteger )
        	->SetCount( sizeof(indices)/sizeof(indices[0]) )
        	->SetValuePointer( indices ) );
	blue_mesh_args.Add(
		NSI::Argument::New( "P" )
        	->SetType( NSITypePoint )
        	->SetCount( sizeof(p2)/sizeof(p2[0]) )
        	->SetValuePointer( const_cast<float*>(p2) ) );

	nsi.Create( "blue", "mesh" );
	nsi.SetAttribute( "blue", blue_mesh_args );
	nsi.Connect( "blue", "", "anchor", "objects" );

	/* white light source (behind camera) */
	float p3[] = {
		-0.5, -0.5, -0.5,  0.5, -0.5, -0.5,  0.5, 0.5, -0.5,  -0.5, 0.5, -0.5 };
	nsi.Create( "white_area_light", "mesh" );
	nsi.SetAttribute( "white_area_light",
		(
			NSI::IntegerArg( "nvertices", 4 ),
			*NSI::Argument::New("P")->SetType(NSITypePoint)->SetCount(4)->SetValuePointer(p3)
		) );
	nsi.Connect( "white_area_light", "", ".root", "objects" );

	/* top light. We ue a LUA script to make transforms */
	float rotate[3] = { 90.0, 0, 0 };
	float scale[3] = { 0.1, 0.05, 1};
	float translate[3] = {0, 0.6, -1.25};

	nsi.Evaluate(
		(
			NSI::StringArg("type","lua"),
			NSI::StringArg("filename", "settransform.lua" ),
			NSI::StringArg("handle", "green_light_trs"),
			NSI::VectorArg("rotate", rotate),
			NSI::VectorArg("scale", scale),
			NSI::VectorArg("translate", translate)
		) );
	nsi.Connect( "green_light_trs", "", "anchor", "objects" );

	/* NSI greenish light source (visible from camera) */
	float p5[] = {-1, -1, 0,  1, -1, 0,  1, 1, 0,  -1, 1, 0};
	nsi.Create( "green_area_light", "mesh" );
	nsi.SetAttribute( "green_area_light",
		(
			NSI::IntegerArg("nvertices", 4),
			*NSI::Argument::New("P")->SetType(NSITypePoint)->SetCount(4)->SetValuePointer(p5)
		) );
	nsi.Connect( "green_area_light", "", "green_light_trs", "objects" );

	/* Attributes nodes. These are needed for shader assignments */
	nsi.Create( "red_attr", "attributes" );
	nsi.Connect( "red_attr", "", "red", "geometryattributes" );
	nsi.Create( "blue_attr", "attributes" );
	nsi.Connect( "blue_attr", "", "blue", "geometryattributes" );
	nsi.Create( "white_attr", "attributes" );
	nsi.Connect( "white_attr", "", "white_area_light", "geometryattributes" );
	nsi.Create( "green_attr", "attributes" );
	nsi.Connect( "green_attr", "", "green_area_light", "geometryattributes" );

	/** Red matte shader */
	float red_color[3] = {0.9, 0.1, 0.1};
	nsi.Create( "red_matte", "shader" );
	nsi.SetAttribute( "red_matte", NSI::StringArg("shaderfilename","matte") );
	nsi.SetAttribute( "red_matte", NSI::ColorArg("Cs", red_color) );
	nsi.Connect( "red_matte", "", "red_attr", "surfaceshader" );

	/** Blue matte shader (different SetAttribute form than above, as an example) */
	float blue_color[3] = {0.2, 0.2, 0.9};
	nsi.Create( "blue_matte", "shader" );
	nsi.SetAttribute( "blue_matte",
		(
			NSI::StringArg("shaderfilename", "matte"),
			NSI::ColorArg("Cs", blue_color )
		) );
	nsi.Connect( "blue_matte", "", "blue_attr", "surfaceshader" );

	/** White matte shader */
	float white_color[3] = {1.f, 1.f, 1.f};
	nsi.Create( "white_matte", "shader" );
	nsi.SetAttribute( "white_matte",
		(
			NSI::StringArg("shaderfilename", "matte"),
			NSI::ColorArg("Cs", white_color )
		) );

	/** White light shader */
	nsi.Create( "white_light", "shader" );
	nsi.SetAttribute( "white_light",
		(
			NSI::StringArg("shaderfilename", "emitter"),
			NSI::FloatArg("power", 25.f)
		) );
	nsi.Connect( "white_light", "", "white_attr", "surfaceshader" );

	/** Green light shader */
	float green_color[3] = {0.2, 0.9, 0.3};
	nsi.Create( "green_light", "shader" );
	nsi.SetAttribute( "green_light",
		(
			NSI::StringArg("shaderfilename" , "emitter" ),
			NSI::FloatArg("power", 1.8),
			NSI::ColorArg("Cs", green_color)
		) );
	nsi.Connect( "green_light", "", "green_attr", "surfaceshader" );

	/** floor */
	float p4[] = {-2, -1, 3,  2, -1, 3,  2, -1, 1,  -2, -1, 1};
	nsi.Create( "floor", "mesh" );
	nsi.SetAttribute( "floor",
		(
			NSI::IntegerArg("nvertices", 4),
			*NSI::Argument::New("P")
				->SetType(NSITypePoint)
				->SetCount(4)
				->SetValuePointer(p4)
		) );

	nsi.Connect( "floor", "", ".root", "objects" );
	nsi.Create( "floor_attributes", "attributes" );
	nsi.Connect( "white_matte", "", "floor_attributes", "surfaceshader" );
	nsi.Connect( "floor_attributes", "", "floor", "geometryattributes" );

	/* START RENDER */
	nsi.RenderControl( NSI::StringArg("action", "start") );
	nsi.RenderControl( NSI::StringArg("action", "wait") );

	nsi.End(); /* good bye boss */

	return 0;
}
