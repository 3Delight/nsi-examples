function identity_matrix()
	return { 1, 0, 0, 0,  0, 1, 0, 0,  0, 0, 1, 0,  0, 0, 0, 1 }
end

function translation_matrix( v )
	return { 1, 0, 0, 0,  0, 1, 0, 0,  0, 0, 1, 0,  v[1], v[2], v[3], 1 }
end

function scale_matrix( v )
	if type(v) == "table" then
		return { v[1], 0, 0, 0,  0, v[2], 0, 0,  0, 0, v[3], 0,  0, 0, 0, 1 }
	else
		return { v, 0, 0, 0,  0, v, 0, 0,  0, 0, v, 0,  0, 0, 0, 1 }
	end
end

-- axis is 0 for x, 1 for y and 2 for z
function rotation_matrix( angle, axis )
	local radian_angle = math.pi * angle / 180
	local s = math.sin( radian_angle )
	local c = math.cos( radian_angle )
	local m = identity_matrix()
	local axis1 = (axis + 1) % 3
	local axis2 = (axis + 2) % 3
	m[axis1 * 4 + axis1 + 1] = c
	m[axis2 * 4 + axis2 + 1] = c
	m[axis1 * 4 + axis2 + 1] = s
	m[axis2 * 4 + axis1 + 1] = -s
	return m
end

function multiply_matrices( m0, m1 )
	local m = {}
	for i = 0, 3 do
		for j = 0, 3 do
			local v = 0
			for k = 0, 3 do
				v = v + m0[i * 4 + k + 1] * m1[k * 4 + j + 1]
			end
			m[i * 4 + j + 1] = v
		end
	end
	return m
end

handle = nsi.scriptparameters.handle.data
nsi.Create( handle, "transform" )

matrix = identity_matrix()

-- Scale
if nsi.scriptparameters.scale ~= nil then
	matrix = multiply_matrices(
		matrix,
		scale_matrix( nsi.scriptparameters.scale.data ) )
end

-- Rotate (xyz for now)
if nsi.scriptparameters.rotate ~= nil then
	for i = 0, 2 do
		matrix = multiply_matrices(
			matrix,
			rotation_matrix( nsi.scriptparameters.rotate.data[i + 1], i ) )
	end
end

-- Translate
if nsi.scriptparameters.translate ~= nil then
	matrix = multiply_matrices(
		matrix,
		translation_matrix( nsi.scriptparameters.translate.data ) )
end

matrix_attr =
{
	name = "transformationmatrix",
	type = nsi.TypeDoubleMatrix,
	data = matrix
}

-- See if motion blur is desired
if nsi.scriptparameters.time ~= nil then
	nsi.SetAttributeAtTime(
		handle, nsi.scriptparameters.time.data, matrix_attr )
else
	nsi.SetAttribute( handle, matrix_attr )
end
